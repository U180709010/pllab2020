#include <stdio.h>
#include <stdlib.h>

struct employeeData{
    int employee_id;
    char employee_name[20];
    char department[20];
    int birth_year;
    int salary;
};

void menu(void);
void addRecord(FILE *fPtr);
void updateRecord(FILE *fPtr);
void deleteRecord(FILE *fPtr);
void showRecords(FILE *fPtr);
void convertTextFile(FILE *fPtr);

int main()
{
    menu();
    return 0;
}

void convertTextFile(FILE *fPtr){
    FILE *fTxt = fopen("employee.txt","w");
    fPtr = fopen("employee.bin","rb");
    struct employeeData employee;

    fprintf(fTxt,"ID No\tEmployee Name\tDepartment\tBirth Year\t\tSalary\n");
    while(!feof(fPtr)){
        fread(&employee,sizeof(struct employeeData),1,fPtr);
        if(employee.employee_id != 0){
            fprintf(fTxt,"%d\t\t%-15s\t%-15s\t%-10d\t%-10d\n",employee.employee_id,employee.employee_name,employee.department,employee.birth_year,employee.salary);
        }
    }
    fclose(fPtr);
    fclose(fTxt);
}

void deleteRecord(FILE *fPtr){
    fPtr = fopen("employee.bin","rb+");
    struct employeeData employee;
    int id;
    printf("\nEnter id to remove employee record: ");
    scanf("%d",&id);

    fseek(fPtr,sizeof(struct employeeData)*(id-1),SEEK_SET);
    fread(&employee,sizeof(struct employeeData),1,fPtr);

    if(employee.employee_id == 0){
        printf("There is no record with id #%d\n",id);
    }else{
        fseek(fPtr,sizeof(struct employeeData)*(id-1),SEEK_SET);
        employee.employee_id = 0;
        strcpy(employee.employee_name , "");
        strcpy(employee.department , "");
        employee.birth_year = 0;
        employee.salary = 0;
        fwrite(&employee,sizeof(struct employeeData),1,fPtr);
        printf("Employee record with id #%d is removed\n",id);
    }
    fclose(fPtr);
}

void updateRecord(FILE *fPtr){
    fPtr = fopen("employee.bin","rb+");
    struct employeeData employee;
    int id;
    int percentage;
    printf("\nEnter id to update amount of employee: ");
    scanf("%d",&id);

    printf("\nID No\tEmployee Name\tDepartment\tBirth Year\tSalary\n");
    fseek(fPtr,sizeof(struct employeeData)*(id-1),SEEK_SET);
    fread(&employee,sizeof(struct employeeData),1,fPtr);
    printf("%d\t%-10s\t%-10s\t%d\t\t%d\n",employee.employee_id,employee.employee_name,employee.department,employee.birth_year,employee.salary);

    if(employee.employee_id == 0){
        printf("There is no record at %d\n",id);
    }else{
        printf("Enter the percentage of increase for salary: ");
        scanf("%d",&percentage);

        fseek(fPtr,sizeof(struct employeeData)*(id-1),SEEK_SET);
        employee.salary = employee.salary + (employee.salary * percentage / 100);
        fwrite(&employee,sizeof(struct employeeData),1,fPtr);

        printf("\nID No\tEmployee Name\tDepartment\tBirth Year\tSalary\n");
        fseek(fPtr,sizeof(struct employeeData)*(id-1),SEEK_SET);
        fread(&employee,sizeof(struct employeeData),1,fPtr);
        printf("%d\t%-10s\t%-10s\t%d\t\t%d\n",employee.employee_id,employee.employee_name,employee.department,employee.birth_year,employee.salary);
    }
    fclose(fPtr);
}

void addRecord(FILE *fPtr){
    fPtr = fopen("employee.bin","rb+");
    struct employeeData employee;

    printf("\nEnter id to create new employee record: ");
    scanf("%d",&employee.employee_id);
    printf("\nEmployee Name: ");
    scanf("%s",employee.employee_name);
    printf("\nDepartment: ");
    scanf("%s",employee.department);
    printf("\nBirth Year: ");
    scanf("%d",&employee.birth_year);
    printf("\nSalary: ");
    scanf("%d",&employee.salary);

    fseek(fPtr,sizeof(struct employeeData)*(employee.employee_id-1),SEEK_SET);
    fwrite(&employee,sizeof(struct employeeData),1,fPtr);

    printf("\nEmployee with id #%d is recorded",employee.employee_id);
    fclose(fPtr);
}

void showRecords(FILE *fPtr){
    fPtr = fopen("employee.bin","rb");
    struct employeeData employee;

    printf("ID No\tEmployee Name\tDepartment\tBirth Year\tSalary\n");
    while(!feof(fPtr)){
        fread(&employee,sizeof(struct employeeData),1,fPtr);
        if(employee.employee_id != 0){
            printf("%d\t%-10s\t%-10s\t%d\t\t%d\n",employee.employee_id,employee.employee_name,employee.department,employee.birth_year,employee.salary);
        }
    }
    fclose(fPtr);
}

void menu(void){
    FILE *fPtr;
    int choice;
    do{
        printf("EMPLOYEE RECORD SYSTEM\n");
        printf("1 - add new record\n");
        printf("2 - update record\n");
        printf("3 - delete record\n");
        printf("4 - print all records\n");
        printf("5 - save as txt file\n");
        printf("6 - end program\n\n");
        scanf(" %d",&choice);
        switch(choice){
        case 1:
            addRecord(fPtr);
            break;
        case 2:
            updateRecord(fPtr);
            break;
        case 3:
            deleteRecord(fPtr);
            break;
        case 4:
            showRecords(fPtr);
            break;
        case 5:
            convertTextFile(fPtr);
            break;
        default:
            break;
        }
        printf("\n********************************************\n");
    }while(choice != 6);
}





