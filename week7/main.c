#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SIZE 80

void insertStr(char* str,char* insertedStr,int position){
    int newSize = strlen(str) + strlen(insertedStr);
    printf("New size is %d",newSize);
    if(newSize > SIZE){
        printf("Its exceeding 80 chars.");
    }else{
        char newStr[newSize];
        strncpy(newStr, str, position);
        newStr[position] = '\0';
        strcat(newStr, insertedStr);
        strcat(newStr, str + position);
        strcpy(str,newStr);
    }
}

void deleteStr(char* str,char* deletedStr) {
    char* match;
    int len = strlen(deletedStr);
    while ((match = strstr(str, deletedStr))) {
        *match = '\0';
        strcat(str, match+len);
    }
}

void findStr(char* str,char* foundStr){
    int len = strlen(foundStr);
    if(len > strlen(str)){
        printf("Given string must be lesser than source string.");
    }else{
        int i,j,counter;
        int isFound = 0;
        for(i=0;i + len<=strlen(str);i++){
            int check = 1;
            for(j=i,counter=0;j<i+len;j++,counter++){
                if(str[j]!=foundStr[counter]){
                    check = 0;
                    break;
                }
            }
            if(check == 1){
                isFound = 1;
                printf("'%s' found at position %d\n",foundStr,i);
                break;
            }
        }
        if(isFound == 0){
            printf("The string cannot be found.\n");
        }
    }
}

void changeStr(char* str,char choice){
    char argStr[SIZE];
    int position;
    switch(choice){
        case 'D':
            printf("String to delete> ");
            scanf(" %[^\n]%*c",argStr);
            deleteStr(str,argStr);
            break;
        case 'I':
            printf("String to insert> ");
            scanf(" %[^\n]%*c",argStr);
            printf("Position of insertion> ");
            scanf(" %d",&position);
            insertStr(str,argStr,position);
            break;
        case 'F':
            printf("String to find> ");
            scanf(" %[^\n]%*c",argStr);
            findStr(str,argStr);
            break;
        default:
            printf("Please enter a valid option.");
            break;
    }
}

int main()
{
    char str[SIZE];
    char choice;
    printf("Enter the source string:\n");
    scanf("%[^\n]%*c",str);

    printf("Enter D(Delete), I(Insert), F(Find), or Q(Quit)> ");
    scanf(" %c",&choice);
    while(choice != 'Q'){
        changeStr(str,choice);
        printf("New source is: %s",str);
        printf("\nEnter D(Delete), I(Insert), F(Find), or Q(Quit)> ");
        scanf(" %c",&choice);
    }
    return 0;
}
