#include <stdio.h>



int isPrime(int num,int x){
    if(x <= 1){
        return 0;
    }else if(num % x == 0){
        return 1;
    }else{
        return isPrime(num,x-1);
    }
}

int main(){
    int num;
    printf("Enter a number: \n");
    scanf("%d",&num);
    int isP = isPrime(num,num-1);
    if(isP == 1){
        printf("Number is not prime.");
    }else if(isP == 0){
        printf("Number is prime");
    }

    return 0;
}