#include <stdio.h>

int gcd(int num1,int num2){
    int max,min;
    if(num1>num2){
        max = num1;
        min = num2;
    }else{
        max = num2;
        min = num1;
    }
    int kalan = max % min;
    if(kalan == 0){
        return min;
    }else{
        gcd(min,kalan);
    }
}
int main(){
    int num1,num2;
    printf("Enter two numbers: \n");
    scanf("%d",&num1);
    scanf("%d",&num2);
    printf("GCD is: %d\n",gcd(num1,num2));
}